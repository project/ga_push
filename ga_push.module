<?php

/**
 * @file
 * Drupal Module: GA Push.
 */

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

// Methods:
define('GA_PUSH_METHOD_DEFAULT', 'default');
define('GA_PUSH_METHOD_DATALAYER_JS', 'datalayer-js');
define('GA_PUSH_METHOD_GA4MP_PHP', 'ga4mp-php');

// Tracking methods:
define('GA_PUSH_TRACKING_METHOD_UNIVERSAL', 'universal');
define('GA_PUSH_TRACKING_METHOD_GOOGLE_ANALYTICS_4', 'Google Analytics 4');

// Sides:
define('GA_PUSH_CLIENT_SIDE', 'client-side');
define('GA_PUSH_SERVER_SIDE', 'server-side');

// Types:
define('GA_PUSH_TYPE_PAGEVIEW', 'pageview');
define('GA_PUSH_TYPE_EVENT', 'event');
define('GA_PUSH_TYPE_ECOMMERCE', 'ecommerce');
define('GA_PUSH_TYPE_EXCEPTION', 'exception');
define('GA_PUSH_TYPE_SOCIAL', 'social');

/**
 * Implements hook_help().
 */
function ga_push_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // @todo Complete help texts
    case 'help.page.ga_push':
      return '<p>' . t('A module that push Google Analytics events') . '</p>';

    case 'ga_push.settings':
      return '<p>' . t('Default method settings.') . '</p>';
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function ga_push_module_implements_alter(&$implementations, $hook) {
  // Ensure GA Push runs after Google Analytics.
  if ($hook == 'page_alter' && isset($implementations['ga_push'])) {
    $group = $implementations['ga_push'];
    unset($implementations['ga_push']);
    $implementations['ga_push'] = $group;
  }
}

/**
 * Implements hook_page_attachments().
 */
function ga_push_page_attachments(array &$attachments) {
  $script = \Drupal::service('ga_push.datalayer')->generateScript();

  if (!empty($script)) {
    $attachments['#attached']['html_head'][] = [
      [
        '#tag' => 'script',
        '#value' => $script,
        '#weight' => 10,
      ],
      'ga_push_tracking_script',
    ];
  }
}

/**
 * Ga push datalayer push data.
 */
function ga_push_method_datalayer($push, $type) {
  \Drupal::service('ga_push.datalayer')->pushData($push, $type);
}

/**
 * Add a new google analytics tracking push.
 *
 * The behavior of this function depends on the parameters it is called with.
 * The following actions can be performed using this function:
 * - Event tracking
 * - Ecommerce tracking
 * - Social tracking
 * - Pageview tracking
 * - ...
 *
 * For better documentation understanding this function has been divided into
 * several functions and should not be called directly.
 *
 * @param array $push_params
 *   - 'push': The push parameters. The value depends on the $type parameter.
 *   - 'type': (optional) The GA Push type:
 *     - GA_PUSH_TYPE_EVENT (default)
 *     - GA_PUSH_TYPE_ECOMMERCE
 *     - GA_PUSH_TYPE_PAGEVIEW
 *     - GA_PUSH_TYPE_SOCIAL.
 *   - 'method_key': (optional) The method identification
 *     - The globally configured setting
 *     - GA_PUSH_METHOD_DEFAULT (default)
 *
 *   All methods:
 *     - 'context': An array of additional info to be passed to drupal_alter
 *       implementations.
 *
 * @see https://developers.google.com/analytics/devguides/collection/gajs/
 *   - GA_PUSH_METHOD_JS
 *   - Universal Tracking, Measurement Protocol.
 *
 * @see ga_push_add_ecommerce()
 * @see ga_push_add_event()
 * @see ga_push_add_pageview()
 * @see ga_push_add_social()
 */
function ga_push_add(array $push_params, $type = GA_PUSH_TYPE_EVENT, $method_key = NULL) {

  $push = [
    'type' => $type,
    'method_key' => $method_key,
  ];

  $push = array_merge($push, $push_params);

  // Let other modules alter the current push data:
  \Drupal::moduleHandler()->alter('ga_push_add', $push);

  if (is_array($push['params']) && count($push['params'])) {

    // If the select method is not available or is not defined:
    if (is_null($push['method_key'])
      || $push['method_key'] == GA_PUSH_METHOD_DEFAULT
      || !ga_push_method_available($push['method_key'], $push['type'])) {

      $push['method_key'] = \Drupal::config('ga_push.settings')->get('default_method');
    }

    if (!is_null($push['method_key'])) {

      $method = ga_push_get_method($push['method_key']);

      // @todo remove deprecated!
      switch ($push['type']) {
        case GA_PUSH_TYPE_EVENT:
          // If the VALUE argument is not numeric -> 1.
          $push['params']['eventValue'] = isset($push['params']['eventValue']) && is_numeric($push['params']['eventValue']) ? $push['params']['eventValue'] : 1;
          break;
      }

      call_user_func($method['callback'], $push['params'], $push['type']);
    }
    else {
      // @todo watchdog there's no method available.
    }

  }
}

/**
 * Push a GA ecommerce.
 *
 * Ecommerce tracking allows you to measure the number of transactions and
 * revenue that your website generates. On a typical ecommerce site, once a user
 * clicks the "purchase" button in the browser, the user's purchase information
 * is sent to the web server, which carries out the transaction. If successful,
 * the server redirects the user to a "Thank You" or receipt page with
 * transaction details and a receipt of the purchase.
 *
 * @param array $push
 *   An associative array containing:
 *   - trans: array
 *     Transaction data. An associative array containing:
 *     - id:
 *       The transaction ID. (e.g. 1234).
 *     - affiliation:
 *      (optional) The store or affiliation from which this transaction occurred
 *      (e.g. Acme Clothing).
 *     - revenue:
 *       Specifies the total revenue or grand total associated with the
 *       transaction (e.g. 11.99). This value may include shipping, tax costs,
 *       or other adjustments to total revenue that you want to include as part
 *       of your revenue calculations.
 *     - tax:
 *       (optional) Specifies the total tax of the transaction. (e.g. 1.29).
 *     - shipping:
 *       (optional) Specifies the total shipping cost of the transaction.
 *       (e.g. 5).
 *     - city:
 *       (optional) city. Only compatible with Classic Analytics.
 *     - region:
 *       (optional) state or province. Only compatible with Classic Analytics.
 *     - country:
 *       (optional) country. Only compatible with Classic Analytics.
 *     - currency:
 *       currency (ISO 4217).
 *
 *   - items: array
 *     Items data. A multiple associative array containing on each row:
 *     - id:
 *       The transaction ID. This ID is what links items to the transactions to
 *       which they belong. (e.g. 1234)
 *     - name:
 *       The item name. (e.g. Fluffy Pink Bunnies).
 *     - sku:
 *       (optional) Specifies the SKU or item code. (e.g. SKU47).
 *     - category:
 *       (optional) The category to which the item belongs (e.g. Party Toys).
 *     - price:
 *       (optional) The individual, unit, price for each item. (e.g. 11.99).
 *     - quantity:
 *       (optional) The number of units purchased in the transaction.
 *       If a non-integer value is passed into this field (e.g. 1.5), it will be
 *       rounded to the closest integer value.
 *     - currency:
 *       currency (ISO 4217)
 * @param string $method_key
 *   Method key.
 *
 * @see https://developers.google.com/analytics/devguides/collection/analyticsjs/ecommerce
 * @see https://developers.google.com/analytics/devguides/collection/gajs/gaTrackingEcommerce
 * @see ga_push_add()
 *
 * @note Important! The Ecommerce plug-in should not be used alongside the
 * Enhanced Ecommerce (ec.js) plug-in.
 *
 * Example:
 * @code
 *   ga_push_add_ecommerce([
 *     'trans' => [
 *       'id'          => '1234',
 *       'affiliation' => 'Acme Clothing',
 *       'revenue'     => '11.99',
 *       'tax'         => '1.29',
 *       'shipping'    => '5',
 *       'currency'    => 'EUR',
 *       'city'        => 'San Jose',  // Classic Analytics only.
 *       'region'      => 'California',  // Classic Analytics only.
 *       'country'     => 'USA',  // Classic Analytics only.
 *     ],
 *     'items' => [
 *       [
 *         'id' => '1234',
 *         'sku'      => 'DD44',
 *         'name'     => 'T-Shirt',
 *         'category' => 'Green Medium',
 *         'price'    => '11.99',
 *         'quantity' => '1',
 *         'currency' => 'EUR'
 *       ],
 *     ],
 *   ]);
 * @endcode
 */
function ga_push_add_ecommerce(array $push, $method_key = NULL) {
  ga_push_add($push, GA_PUSH_TYPE_ECOMMERCE, $method_key);
}

/**
 * Push a GA event.
 *
 * Event tracking allows you to measure how users interact with the content of
 * your website. For example, you might want to measure how many times a button
 * was pressed, or how many times a particular item was used in a web game.
 *
 * @param array $push
 *   An associative array containing:
 *   - eventCategory: string
 *     Typically the object that was interacted with (e.g. button).
 *   - eventAction: string
 *     The type of interaction (e.g. click).
 *   - eventLabel: string
 *     (optional) Useful for categorizing events (e.g. nav buttons).
 *   - eventValue: int
 *     (optional) Values must be non-negative. Useful to pass counts
 *     (e.g. 4 times).
 *   - non-interaction: bool
 *     (optional) A boolean that when set to true, indicates that the event hit
 *     will not be used in bounce-rate calculation. Defaults to FALSE.
 * @param string $method_key
 *   Method key.
 *
 * @see https://developers.google.com/analytics/devguides/collection/analyticsjs/events
 * @see https://developers.google.com/analytics/devguides/collection/gajs/eventTrackerGuide
 * @see ga_push_add()
 *
 * Example:
 * @code
 *   ga_push_add_event([
 *     'eventCategory'        => 'Videos',
 *     'eventAction'          => 'Play',
 *     'eventLabel'           => 'Gone With the Wind',
 *     'eventValue'           => 1,
 *     'nonInteraction'       => FALSE,
 *   ]);
 * @endcode
 */
function ga_push_add_event(array $push, $method_key = NULL) {
  ga_push_add($push, GA_PUSH_TYPE_EVENT, $method_key);
}

/**
 * Push a GA exception.
 *
 * Exception measurement allows you to measure the number and type of crashes
 * and exceptions that occur on your property.
 *
 * @param array $push
 *   An associative array containing:
 *   - exDescription: string
 *     (optional) A description of the exception.
 *   - exFatal: bool
 *     (optional) Indicates whether the exception was fatal. true indicates
 *     fatal.
 * @param string $method_key
 *   Method key.
 *
 * @see https://developers.google.com/analytics/devguides/collection/analyticsjs/exceptions
 * see ga_push_add()
 *
 * Example:
 * @code
 *   ga_push_add_exception([
 *     'exDescription' => 'DatabaseError',
 *     'exFatal'       => FALSE,
 *     'appName'       => 'myApp',
 *     'appVersion'    => 1.0,
 *   ]);
 * @endcode
 */
function ga_push_add_exception(array $push, $method_key = NULL) {
  ga_push_add($push, GA_PUSH_TYPE_EXCEPTION, $method_key);
}

/**
 * Push a GA pageview.
 *
 * Page tracking allows you to measure the number of views you had of a
 * particular page on your web site. For a web app, you might decide to track a
 * page when a large portion of the screen changes, for example when the user
 * goes from the home screen to the settings screen.
 *
 * @param array $push
 *   An associative array containing:
 *   - location: string
 *     (optional) URL of the page being tracked.
 *     By default, analytics_js sets this to the full document URL, excluding
 *     the fragment identifier.
 *   - page: string
 *     (optional) A string that is uniquely paired with each category, and
 *     commonly used to define the type of user interaction for the web object.
 *   - title: string
 *     (optional) The title of the page.
 * @param string $method_key
 *   Method key.
 *
 * @see https://developers.google.com/analytics/devguides/collection/analyticsjs/pages
 * @see https://developers.google.com/analytics/devguides/collection/gajs/
 * @see ga_push_add()
 *
 * Example:
 * @code
 *    ga_push_add_pageview([
 *     'location' => 'https://google.com/analytics',
 *     'page'     => '/my-overridden-page?id=1',
 *     'title'    => 'My overridden page',
 *   ]);
 * @endcode
 */
function ga_push_add_pageview(array $push = [], $method_key = NULL) {
  ga_push_add($push, GA_PUSH_TYPE_PAGEVIEW, $method_key);
}

/**
 * Push a GA social.
 *
 * Social Interaction Analytics allows you to measure the number of times users
 * click on social buttons embedded in webpages. For example, you might measure
 * a Facebook "Like" or a Twitter "Tweet."
 *
 * @param array $push
 *   An associative array containing:
 *   - socialNetwork: string
 *     (optional) The network on which the action occurs (e.g. Facebook,
 *     Twitter).
 *   - socialAction: string
 *     (optional) The type of action that happens (e.g. Like, Send, Tweet).
 *   - socialTarget: string
 *     (optional) Specifies the target of a social interaction. This value is
 *     typically a URL but can be any text.
 * @param string $method_key
 *   Method key.
 *
 * @see https://developers.google.com/analytics/devguides/collection/analyticsjs/social-interactions
 * @see https://developers.google.com/analytics/devguides/collection/gajs/gaTrackingSocial
 * @see ga_push_add()
 *
 * Example:
 * @code
 *   ga_push_add_social([
 *     'socialNetwork' => 'facebook',
 *     'socialAction'  => 'like',
 *     'socialTarget'  => 'http://mycoolpage.com',
 *   ]);
 * @endcode
 */
function ga_push_add_social(array $push, $method_key = NULL) {
  ga_push_add($push, GA_PUSH_TYPE_SOCIAL, $method_key);
}

/**
 * Check if a GA PUSH method is available.
 *
 * @param string $method_key
 *   Method identification.
 * @param string $type
 *   GA Push type.
 *
 * @return bool
 *   Available.
 */
function ga_push_method_available($method_key, $type = NULL) {

  $available = FALSE;
  $methods   = ga_push_get_methods();
  $method    = $methods[$method_key];

  // Checks the method:
  if (is_bool($method['available'])) {
    $available = $method['available'];
  }
  elseif (is_string($method['available'])) {
    $available = call_user_func($method['available']);
  }

  // Checks its implements:
  if (!is_null($type) && $available) {
    $available = $method['implements'][$type] ?? FALSE;
  }

  return $available;
}

/**
 * Retrieves all GA push methods implemented by modules.
 *
 * @return array
 *   An associative array of GA push methods keyed by method name,
 *   each containing the method definition and the implementing module.
 */
function ga_push_get_methods() {
  static $callbacks = [];

  if (empty($callbacks)) {
    $hook = 'ga_push_method';

    // Collect all implementations of the 'ga_push_method' hook.
    \Drupal::moduleHandler()->invokeAllWith($hook, function (callable $hook_function, string $module) use (&$callbacks) {
      $router_items = $hook_function();
      if (is_array($router_items)) {
        foreach ($router_items as $method => $definition) {
          $definition['module'] = $module;
          $callbacks[$method] = $definition;
        }
      }
    });
  }

  return $callbacks;
}

/**
 * Get GA Push method info.
 *
 * @param string $key
 *   Method key.
 */
function ga_push_get_method($key) {
  $methods = ga_push_get_methods();
  return $methods[$key] ?? NULL;
}

/**
 * Returns all available methods as option list.
 */
function ga_push_get_methods_option_list($type = NULL, $default = TRUE) {
  $options = [];
  if ($default) {
    $options['default'] = t('Default');
  }
  $methods = ga_push_get_methods();

  foreach ($methods as $key => $method) {
    if (ga_push_method_available($key, $type)) {
      $options[$key] = $method['name'];
      $options[$key] .= !empty($method['description']) ? ': ' . $method['description'] : '';
      $options[$key] .= ' | ' . $method['side'] . ' (' . $method['tracking_method'] . ' analytics)';
    }
  }
  return $options;
}

/**
 * Get the options list for events.
 */
function ga_push_get_methods_option_list_event() {
  return ga_push_get_methods_option_list(GA_PUSH_TYPE_EVENT);
}

/**
 * Get the options list for exception.
 */
function ga_push_get_methods_option_list_exception() {
  return ga_push_get_methods_option_list(GA_PUSH_TYPE_EXCEPTION);
}

/**
 * Get the options list for page views.
 */
function ga_push_get_methods_option_list_pageview() {
  return ga_push_get_methods_option_list(GA_PUSH_TYPE_PAGEVIEW);
}

/**
 * Get the options list for ecommerce.
 */
function ga_push_get_methods_option_list_ecommerce() {
  return ga_push_get_methods_option_list(GA_PUSH_TYPE_ECOMMERCE);
}

/**
 * Get the options list for page views.
 */
function ga_push_get_methods_option_list_social() {
  return ga_push_get_methods_option_list(GA_PUSH_TYPE_SOCIAL);
}

/**
 * Implements hook_ga_push_method().
 */
function ga_push_ga_push_method() {
  $ga4mpService = \Drupal::service('ga_push.ga4mp');
  $datalayer_service = \Drupal::service('ga_push.datalayer');

  $method[GA_PUSH_METHOD_GA4MP_PHP] = [
    'name' => 'Measurement Protocol GA4(php)',
    'description' => Link::fromTextAndUrl('Measurement Protocol GA4', Url::fromUri('https://developers.google.com/analytics/devguides/collection/protocol/ga4'))->toString(),
    'machine_name' => GA_PUSH_METHOD_GA4MP_PHP,
    'callback' => [$ga4mpService, 'sendEvent'],
    'implements' => [
      GA_PUSH_TYPE_EVENT => TRUE,
      GA_PUSH_TYPE_ECOMMERCE => TRUE,
    ],
    'side' => GA_PUSH_SERVER_SIDE,
    'tracking_method' => GA_PUSH_TRACKING_METHOD_GOOGLE_ANALYTICS_4,
    'available' => 'ga_push_method_GA_GA4_available',
  ];

  $method[GA_PUSH_METHOD_DATALAYER_JS] = [
    'name' => 'Datalayer.js (js)',
    'description' => Link::fromTextAndUrl('Data Layer', Url::fromUri('https://developers.google.com/tag-manager/devguide'))->toString(),
    'machine_name' => GA_PUSH_METHOD_DATALAYER_JS,
    'callback' => [$datalayer_service, 'pushData'],
    'implements' => [
      GA_PUSH_TYPE_EVENT => TRUE,
    ],
    'side' => GA_PUSH_CLIENT_SIDE,
    'tracking_method' => GA_PUSH_TRACKING_METHOD_UNIVERSAL,
    'available' => TRUE,
  ];

  return $method;
}
