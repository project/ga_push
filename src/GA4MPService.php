<?php

namespace Drupal\ga_push;

use Br33f\Ga4\MeasurementProtocol\Dto\Event\AbstractEvent;
use Br33f\Ga4\MeasurementProtocol\Dto\Event\BaseEvent;
use Br33f\Ga4\MeasurementProtocol\Dto\Event\PurchaseEvent;
use Br33f\Ga4\MeasurementProtocol\Dto\Parameter\ItemParameter;
use Br33f\Ga4\MeasurementProtocol\Dto\Request\BaseRequest;
use Br33f\Ga4\MeasurementProtocol\Service;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Handles GA4 Measurement Protocol tracking.
 */
class GA4MPService {

  /**
   * The config factory.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The logger factory.
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * The request stack.
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a new GA4MPService object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    RequestStack $request_stack,
  ) {
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    $this->requestStack = $request_stack;
  }

  /**
   * Sends GA4 event data.
   */
  public function sendEvent(array $event_data, string $event_type): void {
    try {
      $service = $this->initializeService();
      $baseRequest = $this->createBaseRequest();

      $event = $this->createEventObject($event_data);
      $baseRequest->addEvent($event);

      $this->sendRequest($service, $baseRequest, $event_type);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('ga_push')->error('GA4MP error: @error', ['@error' => $e->getMessage()]);
    }
  }

  /**
   * Sends GA4 ecommerce transaction.
   */
  public function sendEcommerceTransaction(array $transaction_data): void {
    try {
      $service = $this->initializeService();
      $baseRequest = $this->createBaseRequest();

      $purchaseEvent = new PurchaseEvent();
      $this->populatePurchaseEvent($purchaseEvent, $transaction_data);
      $baseRequest->addEvent($purchaseEvent);

      $this->sendRequest($service, $baseRequest, GA_PUSH_TYPE_ECOMMERCE);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('ga_push')->error('GA4MP ecommerce error: @error', ['@error' => $e->getMessage()]);
    }
  }

  /**
   * Initializes the GA4 Measurement Protocol service.
   */
  protected function initializeService(): Service {
    if (!class_exists(Service::class)) {
      throw new \RuntimeException('GA4 PHP library not available');
    }

    $config = $this->configFactory->get('ga_push.settings');
    return new Service(
      $config->get('google_analytics_secret'),
      $config->get('google_analytics_4_id')
    );
  }

  /**
   * Creates the base request with client ID.
   */
  protected function createBaseRequest(): BaseRequest {
    $request = new BaseRequest();
    $request->setClientId($this->getClientId());
    return $request;
  }

  /**
   * Gets or generates the client ID.
   */
  protected function getClientId(): string {
    if ($ga_cookie = $this->getGaCookie()) {
      return $ga_cookie;
    }
    return $this->generateUuid();
  }

  /**
   * Gets GA cookie value if available.
   */
  protected function getGaCookie(): ?string {
    $current_request = $this->requestStack->getCurrentRequest();
    if ($current_request instanceof Request && $current_request->cookies->has('_ga')) {
      $ga_cookie = $current_request->cookies->get('_ga');
      [, , $cid1, $cid2] = explode('.', $ga_cookie, 4);
      return $cid1 . '.' . $cid2;
    }
    return NULL;
  }

  /**
   * Generates a UUIDv4 client ID.
   */
  protected function generateUuid(): string {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0x0fff) | 0x4000,
      mt_rand(0, 0x3fff) | 0x8000,
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
  }

  /**
   * Creates the appropriate event object.
   */
  protected function createEventObject(array $event_data): AbstractEvent {
    $eventName = $event_data['eventCategory'] . '_' . $event_data['eventAction'];
    $event = new BaseEvent($eventName);

    foreach ($event_data as $key => $value) {
      $method = 'set' . ucfirst($key);
      if (method_exists($event, $method)) {
        $event->$method($value);
      }
    }

    return $event;
  }

  /**
   * Populates purchase event data.
   */
  protected function populatePurchaseEvent(PurchaseEvent $event, array $transaction_data): void {
    $trans = $transaction_data['trans'];
    $event->setTransactionId($trans['id'])
      ->setAffiliation($trans['affiliation'])
      ->setValue($trans['revenue'])
      ->setShipping($trans['shipping'])
      ->setTax($trans['tax'])
      ->setCurrency($trans['currency']);

    foreach ($transaction_data['items'] as $item) {
      $event->addItem(new ItemParameter([
        'item_id' => $item['sku'],
        'item_name' => $item['name'],
        'item_category' => $item['category'],
        'price' => $item['price'],
        'quantity' => $item['quantity'],
        'currency' => $item['currency'],
      ]));
    }
  }

  /**
   * Sends the actual request to GA4.
   */
  protected function sendRequest(Service $service, BaseRequest $request, string $type): void {
    $debug = $this->configFactory->get('ga_push.settings')->get('debug');
    $logger = $this->loggerFactory->get('ga_push');

    if ($debug) {
      $logger->debug('GA4MP @type request: @data', [
        '@type' => $type,
        '@data' => json_encode($request->export()),
      ]);
    }

    try {
      $response = $service->send($request, $debug);

      if ($debug && $response) {
        $logger->debug('GA4MP @type response: @status - @body', [
          '@type' => $type,
          '@status' => $response->getStatusCode(),
          '@body' => $response->getBody(),
        ]);
      }
    }
    catch (\Exception $e) {
      $logger->error('GA4MP send failed: @error', ['@error' => $e->getMessage()]);
    }
  }

}
