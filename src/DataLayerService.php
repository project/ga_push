<?php

namespace Drupal\ga_push;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Handles DataLayer tracking functionality.
 */
class DataLayerService {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a new DataLayerService object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
  ) {
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Pushes data to the DataLayer queue.
   */
  public function pushData(array $push, string $type) {
    $push_info = [
      'push' => $push,
      'type' => $type,
    ];
    $_SESSION['ga_push_' . GA_PUSH_METHOD_DATALAYER_JS][] = $push_info;
  }

  /**
   * Generates the DataLayer script from queued items.
   */
  public function generateScript(): string {
    $session_key = 'ga_push_' . GA_PUSH_METHOD_DATALAYER_JS;
    $script = "var dataLayer = dataLayer || [];\n";

    if (isset($_SESSION[$session_key])) {
      foreach ($_SESSION[$session_key] as $queued) {
        $script .= $this->generateItemScript($queued['push'], $queued['type']);
      }
      unset($_SESSION[$session_key]);
    }

    return $script;
  }

  /**
   * Generates script code for a single DataLayer item.
   */
  protected function generateItemScript(array $push, string $type): string {
    switch ($type) {
      case GA_PUSH_TYPE_EVENT:
        return $this->generateEventScript($push);

      // Add other types here if needed.
      default:
        $this->loggerFactory->get('ga_push')->warning('Unsupported DataLayer type: @type', ['@type' => $type]);
        return '';
    }
  }

  /**
   * Generates event tracking script.
   */
  protected function generateEventScript(array $push): string {
    $push += ['event' => ''];
    $push = array_filter($push);
    return "dataLayer.push(" . json_encode($push) . ");\n";
  }

}
