<?php

namespace Drupal\ga_push;

/**
 * Interface to define the methods for GA Id tools.
 */
interface GaIdServiceInterface {

  /**
   * Get the Google Analytics ID.
   *
   * Either from google_analytics module service
   * or from our own service.
   */
  public function getAnalyticsId();

  /**
   * Get the Google Analytics 4 ID from configuration.
   */
  public function getAnalytics4Id();

  /**
   * Get the Google Analytics secret.
   */
  public function getAnalyticsSecret();

}
