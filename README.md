# GA PUSH

This module sends data to google analytics and provides an API to integrate with
other modules.

## REQUIREMENTS:

GA4 library br33f/php-ga4-mp https://packagist.org/packages/br33f/php-ga4-mp

## FEATURES:

- Rules integration.
- Browser events.
- Form validate error events.

## API USAGE:

To push a custom query to google analytics see ga_push() function documentation.
